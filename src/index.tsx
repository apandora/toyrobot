import * as React from "react";
import * as ReactDOM from "react-dom";
import AppRouter from "./AppRouter";

import "./styles/global.scss";

ReactDOM.render(
    <AppRouter />,
    document.getElementById("root"),
);