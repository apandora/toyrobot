import * as React from "react";
import { useState } from "react";
import { Box } from "@material-ui/core";
import clsx from "clsx";

import "./style.scss";

const Directions: any = {
  'NORTH': 'north',
  'SOUTH': 'south',
  'EAST': 'east',
  'WEST': 'west',
}

const tileNums: number[] = [0, 1, 2, 3, 4];

const Home = () => {

  const [xPos, setXPos] = useState<number>(-1);
  const [yPos, setYPos] = useState<number>(-1);
  const [dirPos, setDirPos] = useState<string>('');
  const [cfVal, setCFVal] = useState<string>('');
  const [cmdList, setCMDList] = useState<string[]>([]);
  const [invalidCMD, setInvalidCMD] = useState<boolean>(false);
  const [warningCMD, setWarningCMD] = useState<boolean>(false);
  const [outputCMD, setOutputCMD] = useState<boolean>(false);

  const moveRobot = (): void => {
    switch(dirPos) {
      case 'NORTH':
        if(yPos < 4){
          setYPos(yPos+1);
        }
        break;
      case 'SOUTH':
        if(yPos > 0){
          setYPos(yPos-1);
        }
        break;
      case 'EAST':
        if(xPos < 4){
          setXPos(xPos+1);
        }
        break;
      case 'WEST':
        if(xPos > 0){
          setXPos(xPos-1);
        }
        break;
      default:
        break;
    }
  }

  const turnRobotLeft = (): void => {
    switch(dirPos) {
      case 'NORTH':
        setDirPos('WEST');
        break;
      case 'SOUTH':
        setDirPos('EAST');
        break;
      case 'EAST':
        setDirPos('NORTH');
        break;
      case 'WEST':
        setDirPos('SOUTH');
        break;
      default:
        break;
    }
  }

  const turnRobotRight = (): void => {
    switch(dirPos) {
      case 'NORTH':
        setDirPos('EAST');
        break;
      case 'SOUTH':
        setDirPos('WEST');
        break;
      case 'EAST':
        setDirPos('SOUTH');
        break;
      case 'WEST':
        setDirPos('NORTH');
        break;
      default:
        break;
    }
  }

  const processCommand = () : void => {
    let cmdString = cfVal.trim().toUpperCase();
    if(cmdString.length === 0){
      return;
    }

    console.log('command to be processed: ', cmdString);
    if(cmdString.startsWith('PLACE')){
      let arr1 = cmdString.split(" ");
      if(arr1.length === 2){
        let arr2 = arr1[1].split(',');
        if(arr2.length === 3){
          let newX = Number(arr2[0]);
          let newY = Number(arr2[1]);
          let newDir = arr2[2].trim();
          if(!(newX in tileNums) || !(newX in tileNums) || !(Directions.hasOwnProperty(newDir))){
            setInvalidCMD(true);
            return;
          }

          setCMDList([...cmdList, cmdString]);
          setXPos(newX);
          setYPos(newY);
          setDirPos(newDir);
          setCFVal('');
        }
        else{
          setInvalidCMD(true);
        }
      }
      else{
        setInvalidCMD(true);
      }
    }
    else{
      if(cmdList.length > 0){
        switch(cmdString){
          case 'REPORT':
            setOutputCMD(true);
            setCMDList([...cmdList, cmdString]);
            setCFVal('');
            break;
          case 'MOVE':
            moveRobot();
            setCMDList([...cmdList, cmdString]);
            setCFVal('');
            break;
          case 'LEFT':
            turnRobotLeft();
            setCMDList([...cmdList, cmdString]);
            setCFVal('');
            break;
          case 'RIGHT':
            turnRobotRight();
            setCMDList([...cmdList, cmdString]);
            setCFVal('');
            break;
          default:
            setInvalidCMD(true);
            break;
        }
      }
      else{
        setWarningCMD(true);
      }
    }
  };

  let boardTiles = [];
  for(let y=4; y>=0; y--){
    for(let x=0; x<5; x++){
      boardTiles.push(
        <div id={'tile-'+x+'-'+y} key={'tile-'+x+'-'+y} className={clsx('board-tile')}>
          {
            (yPos === y && xPos === x) ?
            (
              <img className={clsx('robot', Directions[dirPos])} src="/images/robot.png" alt="robot" />
            ) 
            : null
          }
          
        </div>
      )
    }
  }

  return (
    <Box className={clsx('main-content')}>
      <Box className={clsx('side-panel')}>
        <input 
          className={clsx('commandfield')} 
          value={cfVal}
          onChange={(e) => {
            setCFVal(e.target.value);
          }}
          onKeyPress={(e) => {
            if (e.which === 13 || e.keyCode === 13) {
                setOutputCMD(false);
                setWarningCMD(false);
                setInvalidCMD(false);
                processCommand();
                return false;
            }
            return true;
          }}
          type="text" 
          placeholder="type command here..." />
        {
          invalidCMD ? <p className={clsx('invalid-cmd-msg')}>* invalid command</p> : null
        }
        {
          warningCMD ? <p className={clsx('warning-cmd-msg')}>* please start with PLACE X,Y,F</p> : null
        }
        {
          outputCMD ? <p className={clsx('output-cmd-msg')}>Current Position: ({xPos}, {yPos}, {dirPos})</p> : null
        }
        <div className={clsx('command-list-box')}>
          <ul className={clsx('command-list')}>
            {
              cmdList.map(item => {
                return (
                  <li>{item}</li>
                );
              })
            }
          </ul>
        </div>
      </Box>
      <Box className={clsx('main-panel')}>
        <div className={clsx('board')}>
          {
            boardTiles
          }
        </div>
      </Box>
    </Box>
  );
};

export default Home;
